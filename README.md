# SMS discussion simulation

Cette page web simule l'apparition de messages dans une conversation type SMS. 

Résultat en démo ici : https://giloop-projects.gitlab.io/sms/

La discussion est définie dans le fichier `discussion.txt` au format : 

```js
[
    ["me", "Salut Fix", 0],
    ["you", "Yep", 1.5],
    ["me", "Est-ce que ça irait ?", 3],
    ["you", "Peut être, j'en sais rien moi",2],
    ["me", "Eh ben tu me redis comment faut adapter tout ça", 5],
    ["you", "Ça marche", 2],
    ["you", "C'est cool Raoul", 1],
    ["me", "Un peu mon neveu", 3],
    ["me", "Bisous 😘 et slip de bain 🩲", 1.5]
]
```

Avec chaque ligne est un message qui contient : `["qui", "Message", durée d'attente]`

Il est possible de personnaliser les icones des personnes soit avec :

- une image 
- une lettre + couleur

Voir les commentaires dans `script.js` 
