function creerAvatar(lettre, couleur = null, taille = 50) {
    if (couleur==null) {
        couleur = `rgb(${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)})`
    }
    var strSVG = `<svg xmlns="http://www.w3.org/2000/svg" class="rounded" width="${taille}" height="${taille}"><rect width="${taille}" height="${taille}" style="fill:${couleur}"></rect><text x="${taille/2-5}" y="${taille/2+5}" fill="white">${lettre}</text></svg>`;
    // Encode la chaîne en base 64 pour la mettre en src de balise d'image
    return ('data:image/svg+xml;base64,' + btoa(strSVG));
}

var me = {};
me.avatar = creerAvatar("M", "rgb(50,0,255)"); // "user-1.png";

var you = {};
you.avatar = creerAvatar("Y", "rgb(234,0,255)"); // "user-2.jpg";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- time = timeout before showing text
function insertChat(who, text, time){
    if (time === undefined){
        time = 0;
    }
    time = time * 1000;
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        control = '<li style="width:100%">' +
                        '<div class="msj macro">' +
                        '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ me.avatar +'" /></div>' +
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="'+you.avatar+'" /></div>' +                                
                  '</li>';
    }
    setTimeout(
        function(){                        
            $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
        }, time);
    
}

function resetChat(){
    $("ul").empty();
}

$(".mytext").on("keydown", function(e){
    if (e.which == 13){
        var text = $(this).val();
        if (text !== ""){
            insertChat("me", text);              
            $(this).val('');
        }
    }
});

$('body > div > div > div:nth-child(2) > span').click(function(){
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
})

//-- Clear Chat
resetChat();

//-- Print Messages
fetch('./discussion.txt').then(response => {
    //console.log(response.json())
    return response.json();
  }).then(data => {
    // Work with JSON data here
    var discussion = data['discussion']
    var cumulTime = 0
    for (let idx = 0; idx < discussion.length; idx++) {
        cumulTime = cumulTime + discussion[idx][2]
        discussion[idx][2] = cumulTime;
    }
    console.log(discussion)
    discussion.map(el => insertChat(el[0], el[1], el[2]));
  }).catch(err => {
    insertChat('me', 'Erreur : problème de chargement du fichier de discussion 😘 ...', 1)
  });

// var tabMessages = [["me", "Salut Fix", 0],
// ["you", "Yep", 1.5],
// ["me", "Est-ce que ça irait ?", 3.5],
// ["you", "Peut être, j'en sais rien moi",7],
// ["me", "Eh ben tu me redis comment faut adapter tout ça", 9.5],
// ["you", "Ça marche", 12],
// ["you", "C'est cool Raoul", 14],
// ["me", "Un peu mon neveu", 15],
// ["me", "Bisous 😘 et slip de bain 🩲", 16.5]]
